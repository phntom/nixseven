*** NiX Seven website ***


*Build instructions* (on ubuntu):

Prereqs: boost (1.50.0+), libqt4-dev, cmake, wt

*building boost:*

	http://sourceforge.net/projects/boost/files/boost/1.50.0/
	chmod +x ./tools/build/v2/engine/build.sh
	sh bootstrap.sh
	./b2
	./b2 install

*building qt:*

	apt-get install libqt4-dev

*building cmake:*

	apt-get install cmake cmake-curses-gui

*building wt:*

	git clone https://github.com/kdeforche/wt.git
	cd wt/src/web
	wget "http://bugs.debian.org/cgi-bin/bugreport.cgi?msg=15;filename=boost1.48-ftbfs.dpatch;att=1;bug=653807" -O boost1.48-ftbfs.dpatch
	patch boost1.48-ftbfs.dpatch
	cd ../..
	mkdir build
	cd build
	cmake ../
	ccmake .      (use c g to exit and save)
	make
	make install

