TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += \
    account.cpp \
    accountregister.cpp




LIBS += -L/opt/local/wt/lib/ -lwt -lwtext -lwthttp

INCLUDEPATH += /opt/local/wt/include
DEPENDPATH += /opt/local/wt/include


LIBS += -L/usr/local/lib/ -lboost_system -lboost_signals

INCLUDEPATH += /usr/local/include
DEPENDPATH += /usr/local/include

HEADERS += \
    accountregister.h

OTHER_FILES += \
    account.xml
