#ifndef ACCOUNTREGISTER_H
#define ACCOUNTREGISTER_H

#include <Wt/WApplication>
#include <Wt/WText>
#include <Wt/WStringUtil>
#include <Wt/WTable>
#include <Wt/WLabel>
#include <Wt/WLineEdit>
#include <Wt/WPushButton>
#include <Wt/WRegExpValidator>
#include <Wt/Auth/PasswordStrengthValidator>

using namespace Wt;

class AccountRegister : public WTable
{
public:
    AccountRegister(WContainerWidget *parent = 0);

protected:
    void submit();
    void createUI();
    void nicknameValidationFeedback();
    void onPassword1Changed();
    void onPassword2Changed();

private:
    WLineEdit *txtNickname;
    WLabel *lblNicknameFeedback;
    WLabel *lblPasswordFeedback;
    WRegExpValidator *valNicknameValidator;

    WLineEdit *txtPassword1;
    WLineEdit *txtPassword2;
    WLineEdit *txtEmail;
    WLineEdit *txtCaptcha;
    WPushButton *btnSubmit;
    Auth::PasswordStrengthValidator *valPassword1Validator;
    Auth::PasswordStrengthValidator *valPassword2Validator;
    WRegExpValidator *valEmailValidator;
    WValidator *valCaptchaValidator;
};

#endif // ACCOUNTREGISTER_H
