#include "accountregister.h"

AccountRegister::AccountRegister(WContainerWidget *parent)
    : WTable(parent)
{
    createUI();
}

void AccountRegister::createUI() {
    WLabel *label;
    int row = 0;

    // title
    WTableCell *cell = elementAt(row, 0);
    cell->setColumnSpan(3);
    cell->setContentAlignment(AlignTop | AlignCenter);
    cell->setPadding(10);
    WText *title = new WText(tr("register.title"), cell);
    title->decorationStyle().font().setSize(WFont::XLarge);

    row++;
    label = new WLabel(tr("register.nickname"), elementAt(row, 0));
    txtNickname = new WLineEdit(elementAt(row, 1));
    txtNickname->setMaxLength(30);
    txtNickname->setFocus();
    txtNickname->setWidth(120);
    elementAt(row, 1)->setColumnSpan(2);
    valNicknameValidator = new WRegExpValidator(this);
    valNicknameValidator->setMandatory(true);
    valNicknameValidator->setRegExp("^[A-}][A-}0-9\\-]{0,29}$");
    row++;
    cell = elementAt(row, 0);
    lblNicknameFeedback = new WLabel("", cell);
    lblNicknameFeedback->setStyleClass("feedback");
    cell->setColumnSpan(3);
    cell->setHidden(true);
    txtNickname->setValidator(valNicknameValidator);
    txtNickname->changed().connect(this, &AccountRegister::nicknameValidationFeedback);

    row++;
    label = new WLabel(tr("register.password"), elementAt(row, 0));
    txtPassword1 = new WLineEdit(elementAt(row, 1));
    txtPassword2 = new WLineEdit(elementAt(row, 2));
    txtPassword1->setEchoMode(WLineEdit::Password);
    txtPassword2->setEchoMode(WLineEdit::Password);
    valPassword1Validator = new Auth::PasswordStrengthValidator();
    valPassword1Validator->setMandatory(true);
    valPassword2Validator = new Auth::PasswordStrengthValidator();
    valPassword2Validator->setMandatory(true);
    txtPassword1->setValidator(valPassword1Validator);
    txtPassword2->setValidator(valPassword2Validator);
    row++;
    cell = elementAt(row, 0);
    lblPasswordFeedback = new WLabel("", cell);
    lblPasswordFeedback->setStyleClass("feedback");
    cell->setColumnSpan(3);
    cell->setHidden(true);
    txtPassword1->changed().connect(this, &AccountRegister::onPassword1Changed);

    row++;
    label = new WLabel(tr("register.email"), elementAt(row, 0));
    txtEmail = new WLineEdit(elementAt(row, 1));
    elementAt(row, 1)->setColumnSpan(2);
    valEmailValidator = new WRegExpValidator(this);
    valEmailValidator->setMandatory(true);
    txtEmail->setValidator(valEmailValidator);

    row++;
    label = new WLabel(tr("register.captcha"), elementAt(row, 0));
//    new WLineEdit(elementAt(row, 1));
    txtCaptcha = new WLineEdit(elementAt(row, 1));
    elementAt(row, 1)->setColumnSpan(2);
    valCaptchaValidator = new WValidator(this);
    valCaptchaValidator->setMandatory(true);
    txtCaptcha->setValidator(valCaptchaValidator);

    row++;
    btnSubmit = new WPushButton(tr("register.submit"), elementAt(row, 0));
    elementAt(row, 0)->setColumnSpan(2);
    elementAt(row, 0)->setContentAlignment(AlignTop | AlignCenter);
    btnSubmit->setMargin(15, Top);
    btnSubmit->clicked().connect(this, &AccountRegister::submit);

}

void AccountRegister::nicknameValidationFeedback() {
    WTableCell *cell = elementAt(2, 0);
    if (txtNickname->validate() == WValidator::Valid) {
        if (false) { //TODO: check if nickname is registered
            lblNicknameFeedback->setText(tr("register.nicktaken"));
            cell->setHidden(false);
        } else {
            cell->setHidden(true);
        }
    } else {
        lblNicknameFeedback->setText(tr("register.nickmalformed"));
        cell->setHidden(false);
    }
}

void AccountRegister::onPassword1Changed() {
    if (txtPassword1->text().value().length() < 6) {
        lblPasswordFeedback->setText(tr("register.passwordshort"));
//
    }
}

void AccountRegister::submit() {
    btnSubmit->setDisabled(true);
}
