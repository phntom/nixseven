#include <Wt/WApplication>

#include "accountregister.h"

using namespace Wt;

WApplication *createApplication(const WEnvironment& env)
{
  WApplication *app = new WApplication(env);
  app->messageResourceBundle().use(WApplication::appRoot() + "account");

  app->setTitle(WWidget::tr("account.apptitle"));

  app->root()->addWidget(new AccountRegister(app->root()));

  return app;
}

int main(int argc, char **argv)
{
    return Wt::WRun(argc, argv, &createApplication);
}
